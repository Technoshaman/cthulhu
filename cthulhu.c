/*******************Technoshaman Feb 22 2013************************

TODO:

- Add IR Ranger + Battery Monitor (ADC) and Compass (i2c) functions
- Use epoll() instead of select()?
- Due to CONFIG_WATCHDOG_NOWAYOUT=y - Use below for development
int WatchDogTimeout = 86400;	// 24 hours
ioctl(WatchDog, WDIOC_SETTIMEOUT, &WatchDogTimeout);

Notes:

- Vbat = Vcc x (ADC reading)/(2^ADC resolution)

- The accelerometer readings are a 12-bit reading "left-justified" in
a 16 bit space, right shift it by 4.

- Setup GPIO ports to be able to get RSSI from RF module and
switch cameras

********************************************************************/

#include "util.h"

#define BUF_SIZE 256
#define PWM_STEP 100000 // 100000 for 10 deg steps

//#define CENT_PWM 1430000 // Center - Futaba S3003
//#define LEFT_PWM_T 2230000 // 0 deg
//#define RIGHT_PWM_T 630000 // 180 deg
//#define LEFT_PWM_R 2430000
//#define RIGHT_PWM_R 530000

#define CENT_PWM 1530000 // Camera Mount Servos
#define LEFT_PWM_T 1830000 // Pitch
#define RIGHT_PWM_T 1130000
#define LEFT_PWM_R 2030000 // Yaw
#define RIGHT_PWM_R 930000

#define BAUDRATE_GPS B9600
#define BAUDRATE_DPT B4800
#define BAUDRATE_RFM B38400

#define SER_DEVICE_GPS "/dev/ttyO1" // UART1
#define SER_DEVICE_DPT "/dev/ttyO2" // UART2
#define SER_DEVICE_RFM "/dev/ttyO4" // UART4

#define IR_RANGER_A "/sys/devices/platform/omap/tsc/ain3" // Sginal AIN2 - IR 1
#define IR_RANGER_B "/sys/devices/platform/omap/tsc/ain7" // Signal AIN6 - IR 2
#define BATTERY_STATUS "/sys/devices/platform/omap/tsc/ain1" // Signal AIN0 - BAT
#define EL_BATTERY_STATUS "/sys/devices/platform/omap/tsc/ain2" // Signal AIN1 - EAT

bool volatile STOP = false;

int MAX(int a, int b, int c);
void intHandler(int dummy);

int main(void)
{
    bool depth_set = false, temp_set = false;
    unsigned int num, rudder_PWM = 1530000, thruster_PWM = 1530000, cntr = 0, interval = 86400; // 1 day
    int bootstatus, fd_MUX, fd_GPS, fd_DPT, fd_RFM, fd_BAT, fd_EAT, fd_IR1, fd_IR2, fd_WD,ret_val, maxfd, angle_t = 0, angle_r = 0;
    char buff_GPS[80], buff_DPTem[32], buff_RFM[32], buff_BAT[8], buff_EAT[8], buff_IR1[8], buff_IR2[8], buff_angle[32], buff_angle_both[45];
    char *buff_Send, *buff_DPT, *buff_DPTok, *buff_TEM, PWM_T[8], PWM_R[8]; // string to send via RFM, etc.
    struct termios newtio_GPS, newtio_DPT, newtio_RFM;
    struct timeval tv; /* timeout for select */
    fd_set readfs;    /* file descriptor set for select */
    static char buf[BUF_SIZE];

    struct sigaction act;
    act.sa_handler = intHandler;
    sigaction(SIGINT, &act, NULL);

    fd_WD = open("/dev/watchdog", O_RDWR); // initialize watchdog
    if (-1 == fd_WD)
    {
      fprintf(stderr, "Error: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    }

/* Initialize ports on OMAP MUX */

    init_MUX("uart1_txd", "0"); // UART1 TXD GPS
    init_MUX("uart1_rxd", "20"); // UART1 RXD GPS
    init_MUX("spi0_d0", "1"); // UART2 TXD TRA
    init_MUX("spi0_sclk", "21"); // UART2 RXD TRA
    init_MUX("gpmc_wpn", "6"); // UART4 TXD RFM
    init_MUX("gpmc_wait0", "26"); // UART4 RXD RFM
    init_MUX("mcasp0_aclkx", "1"); // ehrpwm0A Thruster
    init_MUX("mcasp0_fsx", "1"); // ehrpwm0B Rudder

/* ADD GPIO SETTINGS HERE */

/* End Init OMAP MUX */

/* Connect to the RTC on i2c-3 */

    fd_MUX = open("/sys/class/i2c-dev/i2c-3/device/new_device", O_WRONLY);  // Create RTC Device
    if(fd_MUX < 0)
    {
	perror("RTC device on i2c-3");
    }
    else
    {
	write(fd_MUX, "ds1307 0x68", 11);
	close (fd_MUX);
	system("hwclock -s -f /dev/rtc1 --localtime -D"); // Set local time & date to RTC on i2c-3
    }

/* End RTC Init */

/* Termios settings for RFM, GPS, and DPT ------------------------------------*/

    fd_RFM = open(SER_DEVICE_RFM, O_RDWR | O_NOCTTY);
    if(fd_RFM < 0)
    {
        perror(SER_DEVICE_RFM);
        exit(-1);
    }

    bzero(&newtio_RFM, sizeof(newtio_RFM));

    newtio_RFM.c_cflag |= (BAUDRATE_RFM | CLOCAL | CREAD | CS8);
    newtio_RFM.c_iflag |= (IGNBRK | IGNPAR | IGNCR);
    newtio_RFM.c_lflag |= (ICANON);

/*----------------------------------------------------------------------------*/

    fd_GPS = open(SER_DEVICE_GPS, O_RDWR | O_NOCTTY); // Open serial port
    if(fd_GPS < 0)
    {
        perror(SER_DEVICE_GPS);
        exit(-1);
    }

    bzero(&newtio_GPS, sizeof(newtio_GPS)); // Clear string for newtio

    newtio_GPS.c_cflag |= (BAUDRATE_GPS | CLOCAL | CREAD | CS8); // Control Mode Flags
    newtio_GPS.c_iflag |= (IGNBRK | IGNPAR | IGNCR); // Input Mode Flags
    newtio_GPS.c_lflag |= (ICANON); // Local Mode Flags

/*----------------------------------------------------------------------------*/

    fd_DPT = open(SER_DEVICE_DPT, O_RDWR | O_NOCTTY);
    if(fd_DPT < 0)
    {
        perror(SER_DEVICE_DPT);
        exit(-1);
    }

    bzero(&newtio_DPT, sizeof(newtio_DPT));

    newtio_DPT.c_cflag |= (BAUDRATE_DPT | CLOCAL | CREAD | CS8);
    newtio_DPT.c_iflag |= (IGNBRK | IGNPAR | IGNCR);
    newtio_DPT.c_lflag |= (ICANON);

/* End Termios Settings ------------------------------------------------------*/

/* Init AIN0, AIN1, AIN2, AIN6, IR1, IR2 */

    fd_BAT = open(BATTERY_STATUS, O_RDONLY);
    if(fd_BAT < 0)
    {
        perror(BATTERY_STATUS);
        exit(-1);
    }

    fd_EAT = open(EL_BATTERY_STATUS, O_RDONLY);
    if(fd_EAT < 0)
    {
        perror(EL_BATTERY_STATUS);
        exit(-1);
    }

    fd_IR1 = open(IR_RANGER_A, O_RDONLY);
    if(fd_IR1 < 0)
    {
        perror(IR_RANGER_A);;
        exit(-1);
    }

    fd_IR2 = open(IR_RANGER_B, O_RDONLY);
    if(fd_IR2 < 0)
    {
        perror(IR_RANGER_B);
        exit(-1);
    }

/* END Init */

    printf("\n-------------------------\n");
    printf("Testing servos:\n\n");
    set_PWM("0/", "request", "1"); // PWM-A for Thruster
    set_PWM("0/", "period_ns", "20000000"); // 20000000ns = 20ms, common to both A and B
    set_PWM("0/", "duty_ns", "1530000"); // DC for servo center
    printf("Centering thruster servo...\n");
    usleep(250000);
    set_PWM("0/", "run", "1"); // Go!

    usleep(250000);

    printf("Centering rudder servo...\n");
    usleep(250000);
    set_PWM("1/", "request", "1"); // PWM-B for Rudder
    set_PWM("1/", "duty_ns", "1530000");
    set_PWM("1/", "run", "1");
    printf("\nDone!\n");
    printf("-------------------------\n\n");

    usleep(500000);
//  printf("\033[2J\033[1;1H"); // Clear screen

    tcflush(fd_RFM, TCIFLUSH); tcsetattr(fd_RFM, TCSANOW, &newtio_RFM);
    tcflush(fd_GPS, TCIFLUSH); tcsetattr(fd_GPS, TCSANOW, &newtio_GPS); // Flush received and not read data, set and enable serial ports
    tcflush(fd_DPT, TCIFLUSH); tcsetattr(fd_DPT, TCSANOW, &newtio_DPT);

/* Check if last boot is caused by watchdog */

/*if (ioctl(fd_WD, WDIOC_GETBOOTSTATUS, &bootstatus) == 0)
{
	char *stat;
	stat = malloc(10);

	if(bootstatus != 0)
		strcpy(stat, "WatchDog\n");
	else
		strcpy(stat, "PowOnRst\n");

        printf("\n%c[%d;%dmLast boot is caused by:%c[%dm %s\n",27, 1, 31, 27, 0, stat);

	write(fd_RFM, stat, strlen(stat)); // minus null pointer
	free(stat);
	stat = NULL;
        lseek(fd_RFM, 0, SEEK_SET);
}
else
{
      fprintf(stderr, "Error: Cannot read watchdog status\n");
      exit(EXIT_FAILURE);
}*/

printf("%c[%d;%dmWATCHDOG INTERVAL:%c[%dm %d seconds\n\n", 27, 1, 31, 27, 0, interval);
if (ioctl(fd_WD, WDIOC_SETTIMEOUT, &interval) != 0)
{
       	fprintf(stderr, "Error: Set watchdog interval failed\n");
	exit(EXIT_FAILURE);
}

/* End WD */

    FILE *pLogFile;
    pLogFile = fopen("/root/GPS_log.txt", "a"); // Open log file to append
    if(setvbuf(pLogFile, buf, _IOLBF, BUF_SIZE) != 0)
	perror("setvbuf()");

    while(STOP == false)
    {
	num = read(fd_BAT, &buff_BAT, sizeof(buff_BAT));
	buff_BAT[num] = 0;
	lseek(fd_BAT, 0, SEEK_SET);

	num = read(fd_EAT, &buff_EAT, sizeof(buff_EAT));
	buff_EAT[num] = 0;
	lseek(fd_EAT, 0, SEEK_SET);

	num = read(fd_IR1, &buff_IR1, sizeof(buff_IR1));
	buff_IR1[num] = 0;
	lseek(fd_IR1, 0, SEEK_SET);

	num = read(fd_IR2, &buff_IR2, sizeof(buff_IR2));
	buff_IR2[num] = 0;
	lseek(fd_IR2, 0, SEEK_SET);

	maxfd = MAX(fd_GPS, fd_RFM, fd_DPT) + 1;

	FD_ZERO(&readfs);
	FD_SET(fd_DPT, &readfs);
	FD_SET(fd_RFM, &readfs);
	FD_SET(fd_GPS, &readfs);

	tv.tv_sec = 1;
	tv.tv_usec = 0; // 1s timeout for select(), good for GPS timing

	ret_val = select(maxfd, &readfs, NULL, NULL, &tv);

	if(ret_val)
	{
/*		if(FD_ISSET(fd_DPT, &readfs))
		{
			num = read(fd_DPT, &buff_DPTem, sizeof(buff_DPTem));
			buff_DPTem[num-1] = 0;

			if(buff_DPTem[4] == 'B') // is it a $SDDBT and depth info exists?
			{
				buff_DPTok = strdup(buff_DPTem);

				buff_DPT = strtok(buff_DPTok,","); // get $SDDBT, send ft for more resolution
				buff_DPT = strtok(NULL, ","); // depth, feet
//				buff_DPT = strtok(NULL, ","); // feet symbol
//				buff_DPT = strtok(NULL, ","); // depth, meters

				free(buff_DPTok);
				buff_DPTok = NULL;
				depth_set = true;
			}
			else if(buff_DPTem[4] == 'T') // is it a $SDMTW and temperature info exists?
			{
				buff_DPTok = strdup(buff_DPTem);

				buff_TEM = strtok(buff_DPTok, ","); // get $SDMTW
		                buff_TEM = strtok(NULL, ","); // temperature in degrees

		                free(buff_DPTok);
		                buff_DPTok = NULL;
				temp_set = true;
		        }
//			else
//				printf("\tFiltered out from XDC: %s\n", buff_DPTem);

			lseek(fd_DPT, 0, SEEK_SET);
		}*/

//		if(FD_ISSET(fd_GPS, &readfs) && depth_set && temp_set) // if buff_DPT and/or buff_TEM are empty, problems arise
		if(FD_ISSET(fd_GPS, &readfs))
		{
			num = read(fd_GPS, &buff_GPS, sizeof(buff_GPS));
			buff_GPS[num-1] = 0; // Remove the \n

			if(buff_GPS[5] == 'C' && num > 35) // is it a $GPRMC and a valid sentence?
			{
				cntr++;
				buff_DPT = "81.7";
				buff_TEM = "23.8";

				buff_Send = malloc(strlen(buff_GPS) + strlen(buff_DPT) + strlen(buff_BAT) + strlen(buff_EAT) + strlen(buff_IR1) + strlen(buff_IR2) + strlen(buff_TEM) + 8);
				if(buff_Send == NULL)
			        {
	        		       perror(buff_Send);
		       		       exit(-1);
				}

				strcpy(buff_Send, buff_GPS);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_DPT);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_BAT);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_EAT);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_IR1);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_IR2);
				strcat(buff_Send, "+");
				strcat(buff_Send, buff_TEM);
				strcat(buff_Send, "\n"); // for cannonical serial port reading on the PC

				fprintf(pLogFile, "%u - %s", cntr, buff_Send);
				printf("%c[%d;%dm%04u%c[%dm - %s", 27, 1, 31, cntr, 27, 0, buff_Send);
				write(fd_RFM, buff_Send, strlen(buff_Send));

				free(buff_Send);
				buff_Send = NULL;

				temp_set = false;
				depth_set = false;
			}
//			else
//				printf("\tFiltered out from GPS: %s\n", buff_GPS);

			lseek(fd_GPS, 0, SEEK_SET);
		}

		if(FD_ISSET(fd_RFM, &readfs))
		{
			num = read(fd_RFM, &buff_RFM, sizeof(buff_RFM));
			buff_RFM[num] = 0;

			printf("%c[%d;%dmRF Message >> %c[%dm%s", 27, 1, 31, 27,0, buff_RFM);
			fprintf(pLogFile, "%s", buff_RFM); // Servos are being used for camera pan tilt for this project

			switch(buff_RFM[0]) // Futaba S3003 Rudder 1430000 - Towardpro MG996R Thruster 1540000
			{
				case 't':
				if(buff_RFM[1] == 'c') // Look straight ahead
				{
					thruster_PWM = CENT_PWM;
					snprintf(PWM_T, 8, "%u", thruster_PWM);
                                        set_PWM_DC("0", PWM_T); // Thruster
					angle_t = (int)((thruster_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmThruster servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_t, thruster_PWM, 27, 0);
					snprintf(buff_angle, 32, "Thruster servo at %d degrees\n", angle_t);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == 'l') // Max left (each have one step less limit for Towardpro MG996R)
				{
					thruster_PWM = LEFT_PWM_T;
					snprintf(PWM_T, 8, "%u", thruster_PWM);
                                        set_PWM_DC("0", PWM_T);
					angle_t = (int)((thruster_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmThruster servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_t, thruster_PWM, 27, 0);
					snprintf(buff_angle, 32, "Thruster servo at %d degrees\n", angle_t);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == 'r') // Max right
				{
					thruster_PWM = RIGHT_PWM_T;
					snprintf(PWM_T, 8, "%u", thruster_PWM);
                                        set_PWM_DC("0", PWM_T);
					angle_t = (int)((thruster_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmThruster servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_t, thruster_PWM, 27, 0);
					snprintf(buff_angle, 32, "Thruster servo at %d degrees\n", angle_t);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == '-') // Left++
				{
					thruster_PWM += PWM_STEP; // 10 degree resolution with 100000
					if(thruster_PWM >= LEFT_PWM_T)
						thruster_PWM = LEFT_PWM_T;

					snprintf(PWM_T, 8, "%u", thruster_PWM);
					set_PWM_DC("0", PWM_T);
					angle_t = (int)((thruster_PWM - 530000) * -0.0001 + 90); // map servo PWM values to -90 to +90

					printf("%c[%d;%dmThruster servo at %d degrees (Pw = %d ns)%c[%dm\n", 27, 1, 36, angle_t, thruster_PWM, 27, 0);
					snprintf(buff_angle, 32, "Thruster servo at %d degrees\n", angle_t);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == '+') // Right++
				{
					thruster_PWM -= PWM_STEP;
					if(thruster_PWM <= RIGHT_PWM_T)
						thruster_PWM = RIGHT_PWM_T;

					snprintf(PWM_T, 8, "%u", thruster_PWM);
					set_PWM_DC("0", PWM_T);
					angle_t = (int)((thruster_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmThruster servo at %d degrees%c[%dm\n", 27, 1, 36, angle_t, 27, 0);
					snprintf(buff_angle, 32, "Thruster servo at %d degrees\n", angle_t);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else
				{
					printf("%c[%d;%dmDo what with thruster servo?%c[%dm\n", 27, 1, 31, 27, 0);
					write(fd_RFM, "Incomplete thruster command\n", 28);
				}
				break;
				case 'r':
				if(buff_RFM[1] == 'c') // Look straight ahead
				{
					rudder_PWM = CENT_PWM;
					snprintf(PWM_R, 8, "%u", rudder_PWM);
                                        set_PWM_DC("1", PWM_R); // Rudder
					angle_r = (int)((rudder_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmRudder servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_r, rudder_PWM, 27, 0);
					snprintf(buff_angle, 30, "Rudder servo at %d degrees\n", angle_r);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == 'l') // Max left
				{
					rudder_PWM = LEFT_PWM_R;
					snprintf(PWM_R, 8, "%u", rudder_PWM);
                                        set_PWM_DC("1", PWM_R);
					angle_r = (int)((rudder_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmRudder servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_r, rudder_PWM, 27, 0);
					snprintf(buff_angle, 30, "Rudder servo at %d degrees\n", angle_r);
					write(fd_RFM, buff_angle, strlen(buff_angle));

				}
				else if(buff_RFM[1] == 'r') // Max right
				{
					rudder_PWM = RIGHT_PWM_R;

					snprintf(PWM_R, 8, "%u", rudder_PWM);
                                        set_PWM_DC("1", PWM_R);
					angle_r = (int)((rudder_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmRudder servo at %d (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_r, rudder_PWM, 27, 0);
					snprintf(buff_angle, 30, "Rudder servo at %d degrees\n", angle_r);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == '-') // Left++
				{
					rudder_PWM += PWM_STEP;
					if(rudder_PWM >= LEFT_PWM_R)
						rudder_PWM = LEFT_PWM_R;

					snprintf(PWM_R, 8, "%u", rudder_PWM);
					set_PWM_DC("1", PWM_R);
					angle_r = (int)((rudder_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmRudder servo at: %d degrees (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_r, rudder_PWM, 27, 0);
					snprintf(buff_angle, 30, "Rudder servo at %d degrees\n", angle_r);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else if(buff_RFM[1] == '+') // Right++
				{
					rudder_PWM -= PWM_STEP;
					if(rudder_PWM <= RIGHT_PWM_R)
						rudder_PWM = RIGHT_PWM_R;

					snprintf(PWM_R, 8, "%u", rudder_PWM);
					set_PWM_DC("1", PWM_R);
					angle_r = (int)((rudder_PWM - 530000) * -0.0001 + 90);

					printf("%c[%d;%dmRudder servo at %d degrees (PW = %d ns)%c[%dm\n", 27, 1, 36, angle_r, rudder_PWM, 27, 0);
					snprintf(buff_angle, 30, "Rudder servo at %d degrees\n", angle_r);
					write(fd_RFM, buff_angle, strlen(buff_angle));
				}
				else
				{
					printf("%c[%d;%dmDo what with rudder servo?%c[%dm\n", 27, 1, 31, 27, 0);
					write(fd_RFM, "Incomplete rudder command\n", 26);
				}
				break;
				case '#':
				printf("%c[%d;%dmSystem rebooting...%c[%dm\n", 27, 1, 36, 27, 0);
				write(fd_RFM, "System rebooting...\n", 20);
				sleep(1);
				system("reboot &");
				break;
				case 'S':
				snprintf(buff_angle_both, 41, "Thruster Servo: %d - Rudder Servo: %d\n", angle_t, angle_r);
				write(fd_RFM, buff_angle_both, strlen(buff_angle_both));
				printf("%c[%d;%dmServo status message sent%c[%dm\n", 27, 1, 36, 27, 0);
				break;
				case '$':
				printf("%c[%d;%dmSystem shutdown...%c[%dm\n", 27, 1, 36, 27, 0);
				write(fd_RFM, "System shutdown...\n", 19);
				sleep(1);
				system("shutdown -h now &");
				break;
				case '!':
				STOP = true;
				break;
				default:
				printf("%c[%d;%dmJunk >:(%c[%dm\n", 27, 1, 31, 27, 0);
				write(fd_RFM, "Unknown command\n", 16);
				break;
			}
		lseek(fd_RFM, 0, SEEK_SET);
		}
	}
	if(ret_val == 0)
	{
		printf("%c[%d;%dmselect()%c[%dm timeout!\n", 27, 1, 31, 27 ,0);
	}
	if(ret_val == -1)
	{
		perror("select()");
	}

	ioctl(fd_WD, WDIOC_KEEPALIVE, NULL); // kick watchdog
    }

    set_PWM("0/", "run", "0"); // Stop and release PWM modules
    set_PWM("1/", "run", "0");
    set_PWM("0/", "request", "0");
    set_PWM("1/", "request", "0");

    write(fd_RFM, "Cthulhu sleeps...\n", 18);


// Not possible due to "CONFIG_WATCHDOG_NOWAYOUT=y" kernel config parameter
/*  ioctl(fd_WD, WDIOC_SETOPTIONS, WDIOS_DISABLECARD);
    write(fd_WD, "V", 1);
    close(fd_WD); */

    close(fd_GPS);
    close(fd_DPT);
    close(fd_RFM);
    fclose(pLogFile);

    printf("\n%c[%d;%dmTech-Ne Received quit command from PC Console.%c[%dm\n", 27, 1, 34, 27, 0);
    printf("%c[%d;%dmAll files closed.%c[%dm\n\n", 27, 1, 34, 27, 0);

    exit(EXIT_SUCCESS);
}

int MAX(int a, int b, int c)
{
     int m = a;
     (m < b) && (m = b);
     (m < c) && (m = c);
     return m;
}

void intHandler(int dummy)
{
    STOP = true;
}
