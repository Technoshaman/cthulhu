#include "util.h"

void init_MUX(const char *pin_file, const char *mode_no)
{
        int fd_MUXF;
        char mux_path[] = "/sys/kernel/debug/omap_mux/"; // Stack Alloc
        char *path_and_file; // Heap Alloc

        path_and_file = malloc((strlen(mux_path) + strlen(pin_file)) + 1);
        if(path_and_file == NULL)
        {
                perror(path_and_file);
                exit(-1);
        }
        strcpy(path_and_file, mux_path);
        strcat(path_and_file, pin_file);

        fd_MUXF = open(path_and_file, O_RDWR);
        if(fd_MUXF < 0)
        {
                perror(path_and_file);
                exit(-1);
        }

        free(path_and_file);
	path_and_file = NULL;
        write(fd_MUXF, mode_no, strlen(mode_no));
        close (fd_MUXF);
}

void set_PWM(const char *pwm_no, const char *func_file, const char *value)
{
    char pwm_path[] = "/sys/class/pwm/ehrpwm.0:";
    char *path_and_file;
    int fd_PWM, no_chr;

    path_and_file = malloc(strlen(pwm_path) + strlen(pwm_no) + strlen(func_file) + 1);
    if(path_and_file == NULL)
    {
        perror(path_and_file);
        exit(-1);
    }

    strcpy(path_and_file, pwm_path);
    strcat(path_and_file, pwm_no);
    strcat(path_and_file, func_file);

    fd_PWM = open(path_and_file, O_RDWR);
    if(fd_PWM < 0)
    {
        perror(path_and_file);
        exit(-1);
    }

    free(path_and_file);
    path_and_file = NULL;
    no_chr = strlen(value);
    write(fd_PWM, value, no_chr);
    close(fd_PWM);
}


void set_PWM_DC(const char *pwm_no, const char *dutyCyc_ns) // 1 = Rudder & 0 = Thruster
{
    char dc_path[34];
    int fd_DC;

    snprintf(dc_path, 34, "/sys/class/pwm/ehrpwm.0:%s/duty_ns", pwm_no);

    fd_DC = open(dc_path, O_RDWR);
    if(fd_DC < 0)
    {
        perror(dc_path);
        exit(-1);
    }

    write(fd_DC, dutyCyc_ns, strlen(dutyCyc_ns));
    close(fd_DC);
}
