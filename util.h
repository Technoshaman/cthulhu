#ifndef UTIL_H_
#define UTIL_H_


#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <stdio.h>
#include <linux/watchdog.h>
#include <errno.h>
#include <signal.h>

void init_MUX(const char *pin_file, const char *mode_no); // Set Pin Mux for required modes
void set_PWM(const char *pwm_no, const char *func_file, const char *value); // Set PWM parameters
void set_PWM_DC(const char *pwm_no, const char *dutyCyc_ns); // Both @ 50Hz (20ms), DC varying from 1 to 2 ms

#endif /*UTIL_H_*/
